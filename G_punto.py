#G. Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función

import csv
import pandas as pd

#abrir archivo

archivo=open("Datos_predios.csv",encoding="UTF-8")

lector=csv.reader(archivo,delimiter=";")

p=[]
contador=0
for linea in lector:
    if linea[2]=="Medellin":
        contador+=1
        if contador<=5:
            if not (linea[0] in p):
                p.append(linea[0])

for empresa in p:
    print(empresa)